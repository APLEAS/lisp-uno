;Andrew Pleas
;CSC 458
;Assignment 4
;UNO


;; Global variables for the UNO game
(defvar *deck* nil)
(defvar *computerhand* nil)
(defvar *playerhand* nil)
(defvar *pile* nil)
(defvar *punishment* 0)
(defvar *playeruno* 0)
(defvar *cpuno* 0)

;;card values number 0-9
;;10-skip, 11-Draw 2, 12-Draw 3, 13-wild, 14-Wild-Draw-4

;;; The four suits: Spades, Diamands, Hearts and Clubs
(defparameter *suits* '(red yellow green blue))
(defparameter *values* '(14 13 12 11 10 9 8 7 6 5 4 3 2 1 0))

(defstruct card suit value)

;;; Create a 108-card deck of cards by a doubly-listed map
(defun make-deck ()
  (mapcan #'(lambda (suit)
              (mapcar #'(lambda (value) (make-card :suit suit :value value))
                *values*))
    *suits*))

;;; Make it, then shuffle it and return it.
(defun make-shuffled-deck ()
  (let ((deck (make-deck)))
    (shuffle deck)
    deck))

(defun shuffle (deck)
  (if (null (cdr deck))
      nil
    (let* ((this-card (car deck))
           (cdr-size (length (cdr deck)))
           (loc-to-swap (random cdr-size))
           (card-to-swap (nth loc-to-swap (cdr deck))))
      (setf (car deck) card-to-swap)
      (setf (nth loc-to-swap (cdr deck)) this-card)
      (shuffle (cdr deck)))))

;;; deals a number of cards to a hand
(defun deal (num)
  (loop for i from 1 to num
  collect  (pop *deck*)))



;;; Print a hand to the standard output
(defun print-hand (hand)
  (mapcar #'(lambda (card)
	      (format t "~%~A of ~A" 
		      (card-value card)
		      (card-suit card)))
	  hand ))

;;removes 4, wild draw 4 from the deck
(defun take-14(deck)
  (loop for i in deck
      if (< (card-value i) 14)
      collect i))

;;removes 4 0's from the deck
(defun take-0(deck)
  (loop for i in deck
      if (> (card-value i) 0)
      collect i))

;;removes 4 Wilds from the deck
(defun take-13(deck)
  (loop for i in deck
      if (not (= (card-value i) 13))
        collect i))


;;calls clear to clear all previously saved game data
;;stores the uno deck in global variable *deck*
;;Deals to the player and the computer
;;Then calls starts the game by calling players-turn

(defun start-uno()
  (clear-all)
  (setf *deck* (make-shuffled-uno-deck))
  (shuffle *deck*)
  (setf *computerhand* (deal 7))
  (setf *playerhand* (deal 7))
  (setf *pile* (deal 1))
  (check-pile)
  (format t "Welcome to UNO") 
  (clear-input)
  (catch 'quit
  (players-turn))
  (return-from start-uno()
    ))

;;Creates a uno deck
;;It makes 2 decks and removes the Wild draw 4's from one of the decks and removes 0's and Wild's from the other 
;;the decks are then appended together to make a full UNO deck of 108 cards

(defun make-shuffled-uno-deck()
  (let ((uno-deck nil) (uno-deck2 nil) (uno-deck3 nil))
    (setf uno-deck2 (make-shuffled-deck))
    (setf uno-deck2 (take-14 uno-deck2))
    (setf uno-deck2 (take-0 uno-deck2))
    (setf uno-deck2 (take-13 uno-deck2))
    (setf uno-deck3 (make-shuffled-deck))

    (setf uno-deck (append uno-deck2 uno-deck3))uno-deck)
  )

;;At the beggining of the game the top of the pile cannot be a special card so 
;;if it is a special card it is removed from the pile and placed in the deck
;;the pile then draws another card until it is not a special card.  
(defun check-pile ()
  (print "changing pile top")
           
              (if (> (card-value (car (last *pile*))) 9) 
                  (progn            
                    (setf *deck* (append *deck* (list *pile*)))
                    (setf *pile* nil)
                    (setf *pile* (deal 1))
                    (check-pile))))
                   
;;clears all previous game data
(defun clear-all()
  (setf *deck* nil)
  (setf *pile* nil)
  (setf *computerhand* nil)
  (setf *playerhand* nil)
  (setf *quit* 0)
  (setf *punishment* 0)
  (clear-input)
  )

;;prints the card to play off of
(defun print-top-pile()
  (let ((p-val (card-value (car (last *pile*)))))
              (cond  

             ((equal p-val 14) (format t "~%Card to play off of    :    ~A Wild Draw 4" (card-suit (car (last *pile*)))))
             ((equal p-val 13) (format t "~%Card to play off of    :    ~A Wild" (card-suit (car (last *pile*)))))
             ((equal p-val 12) (format t "~%Card to play off of    :    ~A Draw 3" (card-suit (car (last *pile*)))))
             ((equal p-val 11) (format t "~%Card to play off of    :    ~A Draw 2" (card-suit (car (last *pile*)))))
             ((equal p-val 10) (format t "~%Card to play off of    :    ~A Skip" (card-suit (car (last *pile*)))))
             (t (format t "~%Card to Play off of    :    ~A ~A"(card-suit (car (last *pile*))) p-val)))))

;;Checks if the computer played a special card by checking if *punishment* is greater than 0 
;;if it does it conditionally chooses a necessary punishment
;;calls print-top-pile to print the top of the pile
;;Shows the user a list of its hand 
;;the user selects a card or option using a number for cards or a letter for an option
;;if the user chooses a card it checks conditionally if that card can be played.
;;if the card has the same color or value as the top card in the pile it can be played
;;if the user enters d it draws a deck from the card
;;if the user enters u it checks if the user has 2 cards in its hand if it does it checks if the hand contains playable cards
;;if it contains playable cards then it changes the *playeruno* variable to 1 meaning the player has called UNO
;;If it does not it tells the player they do not have any playable cards and tells them they need to draw a card.
;;If the hand contains more than 2 cards it tells the player it has too many cards and calls players-turn
;;If the user enters c it checks if the computer hand is equal to 1 and if it is it sets the *cpuno* variable to 1 and makes the computer draw 2
;;If the user enters q it returns-from players-turn


(defun players-turn()
  (cond((= (length *computerhand*) 0) (print "Computer won !!!!")(throw 'quit 'quit)))
  (clear-input)
  (let ((response nil)(c nil) (cnt 0)(color-change nil)(play nil))
        (setf play (playable *playerhand*))
    (if( > *punishment* 0)
     (do
    (cond(( = (card-value (car (last *pile*))) 10)(setf *punishment* 0) (print "Computer:  How do you like being skipped ?!?!>! ") (print-top-pile)(computer-turn)) 
     (( = (card-value (car (last *pile*))) 11) (setf *punishment* 0) (player-draw 2) (players-turn))
                              (( = (card-value (car (last *pile*))) 12) (setf *punishment* 0) (player-draw 3) (players-turn))
          (( = (card-value (car (last *pile*))) 14) (setf *punishment* 0) (player-draw 4) (players-turn)))))
    (print-stats)
    (print-top-pile)
    (print "here are your available cards")
    (loop for card in *playerhand*
          do(let ((c-val (card-value card)))
              (cond
             ((equal c-val 14) (format t "~%~s    :     Wild Draw 4" cnt))
             ((equal c-val 13) (format t "~%~s    :     Wild" cnt))
             ((equal c-val 12) (format t "~%~s    :     ~A Draw 3" cnt (card-suit card)))
             ((equal c-val 11) (format t "~%~s    :     ~A Draw 2" cnt (card-suit card)))
             ((equal c-val 10) (format t "~%~s    :     ~A Skip" cnt (card-suit card)))
             (t (format t "~%~s    :     ~A ~A" cnt (card-suit card) c-val)))(setf cnt (+ cnt 1))))
    (format t "~%d :  draw from deck" )
    (format t "~%q :  quit") 
    (format t "~%u :  UNO")
    (format t "~%c :  Call UNO on the computer")
    (format t "~%Choose a card or an option :  ") 
    (setq response  (read))
    
    (if(equal response 'c)
        (progn
          (cond ((and(< (length *computerhand*) 2)(= *cpuno* 0)) (setf *cpuno* 1) (print "Player:  I am calling UNO on the computer!!!") (computer-draw 2))
          ((>(length *computerhand*) 2) (print "The computer has too many cards for you to call UNO")(players-turn)))))
    (if(equal response 'u)
        (progn
          (cond
           ((and(= (length *playerhand*) 2)(> (length play) 0) ) (print "UNO" )(setf *playeruno* 1)(players-turn))
           ((and(>(length *playerhand*) 2)(> (length play) 0) ) (print "You have too many cards to call UNO")(players-turn))
            (t (print "You do not have any card to play you cannot call UNO you must draw a card") (players-turn)))))

    (if (not (equal response 'q))
        (progn
    (cond
     ((equal response 'd) (player-draw 1) (players-turn)) )
          (cond
           
        ((and (player-playable response) 
               (or(= (card-value (nth response *playerhand*)) 10)(= (card-value (nth response *playerhand*)) 11)(= (card-value (nth response *playerhand*)) 12) ))   
         (add-to-pile (nth response *playerhand*))(setf *punishment* 1)(print "You are playing ")(print (nth response *playerhand*))(remove-from-player response)(computer-turn))
                 
           ((= (card-value (nth response *playerhand*)) 13) (setf color-change (change-color)) (setf c (make-card :suit color-change :value '13)) (print "im made a card")  (add-to-pile c)
             (remove-from-player response) (computer-turn))
    
      ((= (card-value (nth response *playerhand*)) 14) (setf color-change (change-color)) (setf *punishment* 1) (setf c (make-card :suit color-change :value '14)) (print "im made a card")  (add-to-pile c)
             (remove-from-player response) (computer-turn))
   
               ((player-playable response)(add-to-pile (nth response *playerhand*))(remove-from-player response)(computer-turn))
     (t (print "You must choose another card if you do not have a card draw one (d)") (players-turn)))) (throw 'quit 'quit))(print"Thank you for playing uno") (computer-turn)))

;;Input: 0
;;When the user draws any wild they can choose a color by entering a number 1-4
;;the color is returned
(defun change-color()
  (let ((response nil))
  (print "choose a color to change to")
        (format t "%Blue=1  Green =2 Yellow=3 Red=4   :")
        (setq response (read))
        (cond
         ((= response 1) 'BLUE)
         ((= response 2) 'GREEN)
         ((= response 3) 'YELLOW)
         ((= response 2) 'RED))))

;;input: a number
;;draws the specified number of cards from the deck into the players hand
(defun player-draw(num)
   (if (= *playeruno* 1)(setf *playeruno* 0))
  (cond(( <(length *deck*) num)(swap-deck)))
  (dotimes (i num) 
    (setf *playerhand* (append *playerhand* (list (pop *deck*)))))) 


;;input: a number
;;draws the specified number of cards from the deck into the computers hand
(defun computer-draw(num)
  (print "Computer:  I need to draw a card!!")
  (if (= *cpuno* 1)(setf *cpuno* 0))
  (cond(( <(length *deck*) num)(swap-deck)))
  (dotimes (i num) 
    (setf *computerhand* (append *computerhand* (list (pop *deck*))))))
;;Input 0
;;when the deck does not have enough cards to deal it it appends the shuffled pile to it
;;the top of the pile is restored to the pile
(defun swap-deck()
   (let ((swap nil))
     (setf swap (last *pile*))
     (setf *pile* (remove (nth (-(length *pile*) 1) *pile*)*pile*))
     (setf *deck* (append *pile*))
     (shuffle *deck*)
         (setf *pile* swap)))
  

;;input 1: a hand of cards
;;sorts the hand by value from least to greatest
;;returns a copy of the sorted list
(defun sort-by-value(hand)
  (sort (copy-list hand) #'< :key #'card-value))

;;prints the length of the deck of cards, the pile, computer hand and player hand
(defun print-stats ()
  (format t "~%Size of deck: ~A   Size of pile: ~A   Size of computer hand: ~A   Size of player hand: ~A" (length *deck*) (length *pile*) (length *computerhand*) (length *playerhand*)  ))

;;Input 0
;;Changes the color to the color with the most occurances in the computers hand
;;First it loops through the hand and counts each color 
;;If statments determine which color has the highest count
;;return the color with the highest count

(defun best-color-option()
  (let ((cp-red 0)(cp-blue 0)(cp-green 0)(cp-yellow 0))
  (loop for card in *computerhand*
                 do(let ((c-color (card-suit card)))
                     (cond ((equalp c-color 'red) (setf cp-red (+ cp-red 1)))  
                           ((equalp c-color 'blue) (setf cp-blue (+ cp-blue 1)))
                           ((equalp c-color 'green) (setf cp-green (+ cp-green 1)))
                           ((equalp c-color 'yellow) (setf cp-yellow (+ cp-yellow 1))))))
  (let((max 0)(color nil))
    (if (> cp-red max) 
        (progn (setf max cp-red)(setf color 'RED)))
    (if (> cp-blue max) 
        (progn (setf max cp-blue)(setf color 'BLUE)))
    (if (> cp-green max) 
        (progn (setf max cp-green)(setf color 'GREEN)))
    (if (> cp-yellow max) 
        (progn (setf max cp-yellow)(setf color 'YELLOW))) color)))


                         
;;input: 0
;;If punishment is greater than 0 than the computer hand is dealt the appropriate punishment
;;checks if it has any playable cards by calling playable
;;if it recursivly draws a card and begins the method again
;;if it does have playable cards it checks the size of the users hand.
;;The computers hand is sorted from least to greatest by the value of the card
;;if the player hand is greater than 3 it searches for a card from least to greatest and plays the first available card
;;if the player hand is less than 4 then it reverses the computers hand and searches from greatest to least and plays the first playable card

(defun computer-turn()
  (cond((= (length *playerhand*) 0) (setf *quit* 1) (print "You won !!!!")(throw 'quit 'quit)))
   (cond ((and(= (length *playerhand*) 1)(= *playeruno* 0)) (setf *playeruno* 1) (print "computer:  UNO you have to draw 2 cards!!!") (player-draw 2)))
  (let(( play-cards 0) (indx 0)(c nil) (color nil)(rnum 0))
     (if( > *punishment* 0)
         (progn
           (cond
            (( = (card-value (car (last *pile*))) 10)(setf *punishment* 0) (players-turn)) 
     (( = (card-value (car (last *pile*))) 11) (setf *punishment* 0) (computer-draw 2) (computer-turn))
                              (( = (card-value (car (last *pile*))) 12) (setf *punishment* 0) (computer-draw 3) (computer-turn))
                              (( = (card-value (car (last *pile*))) 14) (setf *punishment* 0) (computer-draw 4) (computer-turn)))))        
    (setf play-cards (playable *computerhand* ))
    (if (> (length play-cards) 0)
        (progn
          (talk-trash)
          (setf rnum (random 1000))
          (cond ((and(and (= (length *computerhand*) 2)(= *cpuno* 0))(< rnum 801))(setf *cpuno* 1) (print "Computer:   UNO")))
              
                
          (setf *computerhand* (sort-by-value *computerhand*))
      (if (< (length *playerhand*) 4) 
          (setf *computerhand* (reverse *computerhand* )))
         (loop for card in *computerhand*
                 do(let ((c-val (card-value card)) (c-suit (card-suit card)))
                     (cond   
                      ((= c-val 13)(setf color (best-color-option))(setf c (make-card :suit color :value '13))(add-to-pile c)(remove-from-com indx)(players-turn))
                      ((= c-val 14)(setf color (best-color-option))(setf c (make-card :suit color :value '14))(add-to-pile c)(remove-from-com indx)(setf *punishment* 1)(players-turn))
                      ((and (or (equalp c-suit (card-suit (car ( last *pile*))))(= c-val (card-value(car (last *pile*))))) (or (= c-val 10)(= c-val 11)(= c-val 12)(= c-val 14)))   
                       (add-to-pile card)(setf *punishment* 1)(remove-from-com indx)(players-turn))
                        ((or (equalp c-suit (card-suit (car ( last *pile*))))(= c-val (card-value(car (last *pile*)))))(add-to-pile card)(remove-from-com indx)(players-turn)))(setf indx (+ indx 1))))) 
      (computer-draw 1))(computer-turn)))

;;input 0
;;prints a statement according to the card last played by the user
(defun talk-trash()
    (let ((p-val (card-value (car (last *pile*)))))
              (cond
             ((equal p-val 14) (format t "~%Computer-talk:  So you think your a real tough guy cause you played a Wild Draw 4 you must not have any more ~A cards" (card-suit (nth (- (length *pile*)2) *pile*))))
             ((equal p-val 13) (format t "~%Computer-talk:  You thinking changing the color to ~A will stop me!!" (card-suit (nth (- (length *pile*)2) *pile*))))
             ((equal p-val 12) (format t "~%Computer-talk:  I will get revenge for you making me draw 3 !!!"))
             ((equal p-val 11) (format t "~%Computer-talk:  I will get revenge for you making me draw 2 !!!"))
               ((and (equal p-val 10)(> *punishment* 0)) (format t "~%Computer-talk:  You must be scared of my hand if you have to skip me !!!"))
                ((and (equal p-val 10)(= *punishment* 0)) (format t "~%Computer-talk:  How do you like being skipped ?!?!"))
             (t (format t "~%Computer-talk: ~A of ~A  thats all you got hahahaha !!!!" p-val (card-suit (car (last *pile*))))))))

;;input 1: a number
;;removes the card at the index location of the players hand

(defun remove-from-player(index)
   (setf *playerhand*  (remove (nth index *playerhand*) *playerhand*)))

;;input 1 : a number
;;removes the card at the index location of the computers hand

(defun remove-from-com(index)
  (setf *computerhand*  (remove (nth index *computerhand*) *computerhand*)))

;;Input 1: a card
;;returns true if the card is the same suit or value as the top of the pile card.

(defun player-playable(indx)
   (or (equalp (card-suit (nth indx *playerhand*)) (card-suit (car (last *pile*)) )) 
        (equalp (card-value (nth indx *playerhand*)) (card-value (car (last *pile*))))
     
       ))

;;Input 1: hand of cards
;;returns all cards that are the same suit or value as the top of the pile card.
;;also returns all Wilds

(defun playable(hand)
  (let(( cphand nil)(cnt 0))
    (loop for i in hand
          
    do
          (cond
           ((= (card-value i) 14))
        ((or(equalp (card-suit i) (card-suit (car (last *pile*)) )) 
             (equalp (card-value i) (card-value (car (last *pile*)) ))(> (card-value i) 12)) 
         (setf cphand (append cphand (list i))) (setf cnt (+ cnt 1)))) 
        finally (return (sort-by-value cphand)))))
            

;;input 1 : a card
;;adds a card to the top of the pile to become the next card to play off of
(defun add-to-pile(card)
  (setf *pile* (append *pile* (list card))))
